<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>安装程序 -smallshop</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/public/js/jquery.js"></script>
</head>
<body>
<div class="top">
    <div class="top-logo">
        <img src="images/top-logo.png" height="70">
    </div>
    <div class="top-link">
        <ul>
            <li><a href="http://www.smallshop.com" target="_blank">官方网站</a></li>
            <li><a href="#" target="_blank">QQ群:322257814</a></li>
        </ul>
    </div>
    <div class="top-version">
        <!-- 版本信息 -->
        <h2>smallshop</h2>
    </div>
</div>
<div class="main">
    <div class="pleft">
        <dl class="setpbox t1">
            <dt>安装步骤</dt>
            <dd>
                <ul>
                    <li class="succeed">许可协议</li>
                    <li class="succeed">环境检测</li>
                    <li class="succeed">参数配置</li>
                    <li class="succeed">正在安装</li>
                    <li class="now">安装完成</li>
                </ul>
            </dd>
        </dl>
    </div>
    <div class="pright">
        <!--右边-->
        <form action="" method="get">
            <div class="index_mian_right_one_ly">
                <div class="index_mian_right_one_one_ly"><span>安装完成</span></div>
                <div class="font">恭喜，smallshop安装完成。现在可以：</div>
                <div class="btn">
                    <a href="/manager">
                        <input name="" class="index_mian_right_seven_Forward_ly" type="button" value="后台管理"/>
                    </a>
                    <a href="/">
                        <input name="" class="index_mian_right_seven_Forward_ly" type="button" value="进入首页"/>
                    </a>
                </div>

            </div>
            <!--进入系统-->
            <div class="btnn-box"></div>
        </form>
    </div>
</body>
</html>
