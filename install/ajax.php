<?php
include_once 'common.php';
//数据库和管理员信息
$mysql_host     = trim($_POST['mysql_host']);
$mysql_user     = trim($_POST['mysql_user']);
$mysql_password = trim($_POST['mysql_password']);
$mysql_pre      = trim($_POST['mysql_pre']);
$mysql_name     = trim($_POST['mysql_name']);
$admin_user     = trim($_POST['admin_user']);
$admin_password = trim($_POST['admin_password']);

//连接mysql
$host_data  = explode(":", $mysql_host);
$mysql_port = $host_data[1];
if (empty($mysql_port)) $mysql_port = @ini_get("mysqli.default_port");
$mysql_conn = new mysqli($host_data[0], $mysql_user, $mysql_password, NULL, $mysql_port);

if ($mysql_conn->connect_errno) {
    error_json('数据库连接失败' . $mysql_link->connect_errno);
}

//查看sql文件是否存在
$smallshop_sql_file = dirname(__FILE__) . '/smallshop.sql';
if (!file_exists($smallshop_sql_file)) {
    error_json('数据库安装文件不存在');
}

//检测数据库是否存在
$mysql_conn->set_charset('utf8');
$mysql_conn->query("SET SESSION sql_mode = '' ");
if (version_compare($mysql_conn->server_version, '5.0.0', '<')) {
    error_json('mysql版本不能小于5.0');
}
if ($mysql_conn->select_db($mysql_name) == false) {
    if (!$mysql_conn->query('CREATE DATABASE `' . $mysql_name . '` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci')) {
        error_json('数据库创建失败，请手动创建数据表');
    }
    $mysql_conn->select_db($mysql_name);
}

//开始导入sql
$mysql_conn->query("SET FOREIGN_KEY_CHECKS = 0;");
$sql_data = fopen($smallshop_sql_file, 'r');
while (!feof($sql_data)) {
    $sql_str = fgets($sql_data);
    if (isset($sql_str[0]) && $sql_str[0] != '#') {
        $sql_str_pre = substr($sql_str, 0, 2);
        switch ($sql_str_pre) {
            case '--' :
            case '//' : {
                continue;
            }
            case '/*': {
                if (substr($sql_str, -5) == "*/;\r\n" || substr($sql_str, -4) == "*/\r\n") {
                    continue;
                } else {
                    continue;
                }
            }
            default : {
                $one_sql_data[] = trim($sql_str);
                if (substr(trim($sql_str), -1) == ";") {
                    $query_sql    = str_ireplace("my_", $mysql_pre, join($one_sql_data));
                    $one_sql_data = array();
                    $mysql_conn->query($query_sql);
                }
            }
        }
    }
}
//添加管理员数据
$salt                = rand(100000, 999999);
$admin_user_password = md5(md5($admin_password) . $salt);
$admin_user_sql      = "INSERT INTO `" . $mysql_pre . "admin` (`username`, `password`, `salt`, `full_name`,  `role_id`, `status`, `addtime`)VALUES('" . $admin_user . "','" . $admin_user_password . "','" . $salt . "','管理员',0,0," . time() . ");";

if (!$mysql_conn->query($admin_user_sql)) {
    error_json('创建管理员失败');
}

$database_config = <<<Eof
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
\$active_group = 'default';
\$query_builder = TRUE;

\$db['default'] = array(
	'dsn'	=> '',
	'hostname' => '$mysql_host:$mysql_port',
	'username' => '$mysql_user',
	'password' => '$mysql_password',
	'database' => '$mysql_name',
	'dbdriver' => 'mysqli',
	'dbprefix' => '$mysql_pre',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8mb4',
	'dbcollat' => 'utf8mb4_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
Eof;
if (!file_put_contents(dirname(__FILE__) . '/../application/config/database.php', $database_config)) {
    error_json('配置文件创建失败');
}
file_put_contents(dirname(__FILE__) . '/install.lock', 'smallshop版权所有');
error_json('y');
?>